# Scroller method and parallax/scrolling background
# Python demo. Created by Chris L..
#
# Required keys for method to accept hashes:
#   "posX": Drawing's position
#   "spos": Starting position
#   "mpos": Max position, usually screen height/width
#   "ac": acceleration of image

def scroller(arr):
  for i in arr:
    # Add acceleration to positions
    i["pos1"] += i["ac"]
    i["pos2"] += i["ac"]
    
    # Check if positions exceed max
    # Then bring them back to start if they do
    if i["pos1"] >= i["mpos"]:
      i["pos1"] = i["spos"]
    if i["pos2"] >= i["mpos"]:
      i["pos2"] = i["spos"]
  
  # Return newly created array
  return arr

if __name__ == "__main__":
  import pygame, sys
  
  s_size = width, height = 640, 480
  screen = pygame.display.set_mode(s_size)
  pygame.display.set_caption("Pygame Parallax")
  
  para = [ { "img": pygame.image.load("assets/forest.png"),
  "pos1": 0, "pos2":-480, "spos": -480, "mpos": 480, "ac": 4 },

  { "img": pygame.image.load("assets/fleet.png"),
  "pos1": 0, "pos2":-480, "spos": -480, "mpos": 480, "ac": 8 },

  { "img": pygame.image.load("assets/clouds.png"),
  "pos1": -480, "pos2": -1440, "spos": -1440, "mpos": 480, "ac": 16 } ]

  clock = pygame.time.Clock()
  
  while True:
    # Check for exit
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        sys.exit()
    
    # Get modified array
    para = scroller(para)
    
    # Clear screen
    screen.fill((255, 255, 255))
    
    # Draw the images
    for i in para:
      screen.blit(i["img"], (0, i["pos2"]))
      screen.blit(i["img"], (0, i["pos1"]))
    
    # And, finally, reset display
    pygame.display.flip()
    
    clock.tick(30)
